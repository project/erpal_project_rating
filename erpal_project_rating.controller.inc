<?php
/**
 * RatingEntity controller class.
 */
class RatingEntityController extends EntityAPIController {

  public function create(array $values = array()) {
    $values += array(
      'created' => REQUEST_TIME,
    );
    return parent::create($values);
  }
}

/**
 * RatingEntity class.
 */
class RatingEntity extends Entity {
  protected function defaultLabel() {
    return $this->rating_bundle;
  }
}

/**
 * RatingEntityBundle class.
 */
class RatingEntityBundle extends Entity {
  public $machine_name;
  public $name;
  public $description;
  public $category;
  public $points;
  public $weight = 0;

  public function __construct($values = array()) {
    parent::__construct($values, 'rating_entity_bundle');
  }

  function isLocked() {
    return isset($this->status) && empty($this->is_new) && (($this->status & ENTITY_IN_CODE) || ($this->status & ENTITY_FIXED));
  }
}
