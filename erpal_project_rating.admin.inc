<?php

define('RATING_BUNDLE_ADMIN_PATH', 'admin/structure/project-rating-bundles');
define('RATING_CATEGORY_VOC_NAME', 'project_rating_category');

/**
 * Generates the rating bundle editing form.
 */
function rating_entity_bundle_form($form, &$form_state, $rating_bundle, $op = 'edit') {

  if ($op == 'clone') {
    $rating_bundle->name .= ' (cloned)';
  }

  $form['name'] = array(
    '#title' => t('Bundle name'),
    '#type' => 'textfield',
    '#default_value' => $rating_bundle->name,
    '#description' => t('The human-readable name of this rating bundle.'),
    '#required' => TRUE,
    '#size' => 30,
  );
  $form['machine_name'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($rating_bundle->machine_name) ? $rating_bundle->machine_name : '',
    '#maxlength' => 32,
    '#disabled' => $rating_bundle->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'rating_entity_bundles',
      'source' => array('name'),
    ),
    '#description' => t('A unique machine-readable name for this rating bundle. It must only contain lowercase letters, numbers, and underscores.'),
  );
  $form['description'] = array(
    '#title' => t('Description'),
    '#type' => 'textarea',
    '#default_value' => isset($rating_bundle->description) ? $rating_bundle->description : '',
    '#description' => t('Description about the rating bundle.'),
  );
  $form['category'] = array(
    '#title' => t('Choose category'),
    '#type' => 'select',
    '#default_value' => $rating_bundle->category,
    '#options' => rating_entity_get_category_terms(),
    '#required' => TRUE,
  );
  $form['points'] = array(
    '#title' => t('Amount of points'),
    '#type' => 'textfield',
    '#default_value' => $rating_bundle->points,
    '#description' => t('Amount of points that will receive entity.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save rating bundle'),
    '#weight' => 40,
  );

  if (!$rating_bundle->isLocked() && $op != 'add' && $op != 'clone') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete rating bundle'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('rating_entity_bundle_form_submit_delete')
    );
  }
  return $form;
}

/**
 * Validate callback for creating/editing rating bundle form.
 */
function rating_entity_bundle_form_validate($form, $form_state) {
  if (!is_numeric($form_state['values']['points'])) {
    form_set_error('points', t('Amount of points field must be a positive integer.'));
  }
}

/**
 * Submit handler for creating/editing rating bundle.
 */
function rating_entity_bundle_form_submit(&$form, &$form_state) {
  $rating_bundle = entity_ui_form_submit_build_entity($form, $form_state);
  rating_entity_bundle_save($rating_bundle);
  $form_state['redirect'] = RATING_BUNDLE_ADMIN_PATH;
}

/**
 * Submit handler for delete rating bundle form.
 */
function rating_entity_bundle_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = RATING_BUNDLE_ADMIN_PATH .'/manage/' . $form_state['rating_entity_bundle']->machine_name . '/delete';
}

/**
 * Returns taxonomy terms from default category vocabulary.
 */
function rating_entity_get_category_terms() {
  $vocabulary = taxonomy_vocabulary_machine_name_load(RATING_CATEGORY_VOC_NAME);
  $terms = taxonomy_get_tree($vocabulary->vid);
  $result = array();
  foreach ($terms as $term) {
    $result[$term->tid] = $term->name;
  }
  return $result;
}


